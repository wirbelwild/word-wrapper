<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Tests\Output;

use BitAndBlack\WordWrapper\Output\HTMLOutput;
use BitAndBlack\WordWrapper\Wrapper\LinesWrapper;
use PHPUnit\Framework\TestCase;

class HTMLOutputTest extends TestCase
{
    public function testCanFormat(): void
    {
        $sentence = 'This is a test';
        $wrapper = new LinesWrapper(2, $sentence);
        $output = new HTMLOutput($wrapper);
        
        self::assertSame(
            'This is <br>a test',
            $output->getSentence()
        );
    }
}
