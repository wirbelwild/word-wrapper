<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Tests\Output;

use BitAndBlack\WordWrapper\Output\CustomOutput;
use BitAndBlack\WordWrapper\Wrapper\LinesWrapper;
use PHPUnit\Framework\TestCase;

class CustomOutputTest extends TestCase
{
    public function testCanFormat(): void
    {
        $sentence = 'This is a test';
        $wrapper = new LinesWrapper(2, $sentence);
        $output = new CustomOutput($wrapper, '{{MYCHARACTER}}');
        
        self::assertSame(
            'This is {{MYCHARACTER}}a test',
            $output->getSentence()
        );
    }
}
