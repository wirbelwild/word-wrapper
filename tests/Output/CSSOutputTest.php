<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Tests\Output;

use BitAndBlack\WordWrapper\Output\CSSOutput;
use BitAndBlack\WordWrapper\Wrapper\LinesWrapper;
use PHPUnit\Framework\TestCase;

class CSSOutputTest extends TestCase
{
    public function testCanFormat(): void
    {
        $sentence = 'This is a test';
        $wrapper = new LinesWrapper(2, $sentence);
        $output = new CSSOutput($wrapper);
        
        self::assertSame(
            '<span class="word-wrapper-line">This is </span>\n<span class="word-wrapper-line">a test</span>\n',
            $output->getSentence()
        );
    }
}
