<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Tests\Wrapper;

use BitAndBlack\WordWrapper\Wrapper\LinesWrapper;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class LinesWrapperTest extends TestCase
{
    public static function getCanWrapData(): Generator
    {
//        yield [
//            'This is a test',
//            2,
//            [
//                'This is ',
//                'a test',
//            ],
//        ];
//
//        yield [
//            'This  is     a    test   ',
//            2,
//            [
//                'This  is     ',
//                'a    test   ',
//            ],
//        ];

        yield [
            'Magie Westafrikas: vergangene Königreiche, tropische Landschaften und spektakuläre Zangbeto-Zeremonien erleben',
            3,
            [
                'Magie Westafrikas: vergangene Königreiche, ',
                'tropische Landschaften und spektakuläre ',
                'Zangbeto-Zeremonien erleben',
            ],
        ];

        yield [
            'Ein paar echt sehr kurze Worte gefolgt von unglaublich endlos-langen sinnlosesten Aneinanderreihungen',
            2,
            [
                'Ein paar echt sehr kurze Worte gefolgt von unglaublich ',
                'endlos-langen sinnlosesten Aneinanderreihungen',
            ],
        ];

        yield [
            'Ein paar echt sehr kurze Worte gefolgt von unglaublich endlos-langen sinnlosesten Aneinanderreihungen',
            3,
            [
                'Ein paar echt sehr kurze Worte gefolgt ',
                'von unglaublich endlos-langen sinnlosesten ',
                'Aneinanderreihungen',
            ],
        ];

        yield [
            'Unglaublich endlos-langen sinnloseste Aneinanderreihungen gefolgt von ein paar echt sehr kurzen Worten',
            2,
            [
                'Unglaublich endlos-langen sinnloseste Aneinanderreihungen ',
                'gefolgt von ein paar echt sehr kurzen Worten',
            ],
        ];

        yield [
            'Unglaublich langen Aneinander-Reihungen sinnlosester Art',
            2,
            [
                'Unglaublich langen Aneinander-Reihungen ',
                'sinnlosester Art',
            ],
        ];

        yield [
            'Unglaublich langen Aneinander-Reihungen sinnlosester Art',
            2,
            [
                'Unglaublich langen Aneinander-',
                'Reihungen sinnlosester Art',
            ],
            true,
        ];

        yield [
            'Wüstenritt durch unbekanntes Nomadenland',
            2,
            [
                'Wüstenritt durch ',
                'unbekanntes Nomadenland',
            ],
        ];

        yield [
            'Wandertraum auf der Atlantikinsel São Miguel',
            2,
            [
                'Wandertraum auf der ',
                'Atlantikinsel São Miguel',
            ],
        ];

        yield [
            'Intensives Natur- und Huskyerlebnis im verschneiten Lappland mit drei Nächten in urigen Wildnishütten',
            2,
            [
                'Intensives Natur- und Huskyerlebnis im verschneiten ',
                'Lappland mit drei Nächten in urigen Wildnishütten',
            ],
        ];

        yield [
            'Kultur- und Wüstenreise von Algier über die Dünen des Grand Erg Occidental zu den Hügelstädten der Pentapolis',
            2,
            [
                'Kultur- und Wüstenreise von Algier über die Dünen des ',
                'Grand Erg Occidental zu den Hügelstädten der Pentapolis',
            ],
        ];

        yield [
            'Traumlandschaften des Afrikanischen Grabenbruchs und einmaliges Kulturmosaik des südlichen Omo-Tals',
            2,
            [
                'Traumlandschaften des Afrikanischen Grabenbruchs und ',
                'einmaliges Kulturmosaik des südlichen Omo-Tals',
            ],
        ];

        yield [
            'Wanderreise rund um Viñales, Trinidad und Havanna: Tabakfelder, Wasserfälle, üppige Wälder und Kolonialstädte',
            2,
            [
                'Wanderreise rund um Viñales, Trinidad und Havanna: ',
                'Tabakfelder, Wasserfälle, üppige Wälder und Kolonialstädte',
            ],
        ];

        yield [
            'Wanderreise rund um Viñales, Trinidad und auch noch über Havanna: Tabakfelder, Wasserfälle, üppige Wälder und Kolonialstädte',
            2,
            [
                'Wanderreise rund um Viñales, Trinidad und auch noch über Havanna: ',
                'Tabakfelder, Wasserfälle, üppige Wälder und Kolonialstädte',
            ],
        ];

        yield [
            'Höhepunkte Ägyptens: Vom Schatten der Sphinx über die Säulen von Karnak bis in die Unterwelt im Tal der Könige',
            2,
            [
                'Höhepunkte Ägyptens: Vom Schatten der Sphinx über die ',
                'Säulen von Karnak bis in die Unterwelt im Tal der Könige',
            ],
        ];
    }

    /**
     * @param string $sentence
     * @param int $linesCount
     * @param array<int, string> $sentenceExpected
     * @param bool $allowUsingExistingHyphens
     * @return void
     */
    #[DataProvider('getCanWrapData')]
    public function testCanWrap(
        string $sentence,
        int $linesCount,
        array $sentenceExpected,
        bool $allowUsingExistingHyphens = false,
    ): void {
        $linesWrapper = new LinesWrapper($linesCount, $sentence, $allowUsingExistingHyphens);

        self::assertSame(
            $sentenceExpected,
            $linesWrapper->getSentence()
        );
    }
}
