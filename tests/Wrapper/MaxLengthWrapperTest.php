<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Tests\Wrapper;

use BitAndBlack\WordWrapper\Wrapper\MaxLengthWrapper;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MaxLengthWrapperTest extends TestCase
{
    /**
     * @param string $sentence
     * @param int $maxLength
     * @param array<int, string> $sentenceExpected
     * @return void
     */
    #[DataProvider('getCanWrapData')]
    public function testCanWrap(string $sentence, int $maxLength, array $sentenceExpected): void
    {
        $maxLengthWrapper = new MaxLengthWrapper($maxLength, $sentence);

        self::assertSame(
            $sentenceExpected,
            $maxLengthWrapper->getSentence()
        );

        $countExpected = count($sentenceExpected);

        self::assertSame(
            $countExpected,
            $maxLengthWrapper->getLinesCount(),
        );
    }

    public static function getCanWrapData(): Generator
    {
        yield [
            'This is a test',
            8,
            [
                'This is ',
                'a test',
            ],
        ];
    }
}
