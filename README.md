[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/word-wrapper)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/word-wrapper/v/stable)](https://packagist.org/packages/bitandblack/word-wrapper)
[![Total Downloads](https://poser.pugx.org/bitandblack/word-wrapper/downloads)](https://packagist.org/packages/bitandblack/word-wrapper)
[![License](https://poser.pugx.org/bitandblack/word-wrapper/license)](https://packagist.org/packages/bitandblack/word-wrapper)

# Bit&Black Word Wrapper

Wrapping text for a perfect typography.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/word-wrapper). Add it to your project by running `$ composer require bitandblack/word-wrapper`.

## Usage 

These wrappers are available:

*   `LinesWrapper`: This class divides a text to reach a given number of lines.
*   `MaxLengthWrapper`: This class wraps a text when a lines reaches the maximum length of characters. 

These outputs are available:

*   `CSSOutput`: This class uses `<span>` to divide the lines. Each `span` has `class="word-wrapper-line"` which you can use to define a wrap by your own. For example `white-space: nowrap` is very useful here. It's possible to customize the tag.
*   `HTMLOutput`: This class uses `<br>` to wrap into new lines. It's possible to customize the tag.
*   `CustomOutput`: This class needs to know your custom string. This is the second parameter.

### Example

This example makes use of the `LinesWrapper` and the `HTMLOutput`.

````php
<?php 

use BitAndBlack\WordWrapper\Output\HTMLOutput;
use BitAndBlack\WordWrapper\Wrapper\LinesWrapper;

$sentence = 'The Lord of the Rings';
$wrapper = new LinesWrapper(2, $sentence);
echo new HTMLOutput($wrapper);
````

This will echo `The Lord of <br>the Rings`.

## Help 

If you have questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).