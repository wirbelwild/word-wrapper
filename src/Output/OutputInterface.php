<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Output;

interface OutputInterface
{
    /**
     * Returns the formatted sentence.
     *
     * @return string
     */
    public function getSentence(): string;
}
