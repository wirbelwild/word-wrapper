<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Output;

use BitAndBlack\WordWrapper\Wrapper\WrapperInterface;
use Stringable;

/**
 * @see \BitAndBlack\WordWrapper\Tests\Output\HTMLOutputTest
 */
readonly class HTMLOutput implements OutputInterface, Stringable
{
    public function __construct(
        private WrapperInterface $wrapper,
        private string $character = '<br>'
    ) {
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getSentence();
    }

    /**
     * Returns the formatted sentence.
     */
    public function getSentence(): string
    {
        $sentence = $this->wrapper->getSentence();
        return implode($this->character, $sentence);
    }
}
