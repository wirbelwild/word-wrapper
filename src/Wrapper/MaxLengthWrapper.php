<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Wrapper;

/**
 * @see \BitAndBlack\WordWrapper\Tests\Wrapper\MaxLengthWrapperTest
 */
readonly class MaxLengthWrapper implements WrapperInterface
{
    /**
     * @var array<int, string>
     */
    private array $sentence;

    public function __construct(int $maxLength, string $sentence)
    {
        $sentence = wordwrap($sentence, $maxLength, ' \n');
        $sentenceSplitted = explode('\n', $sentence);
        $this->sentence = $sentenceSplitted;
    }

    /**
     * Returns an array of lines.
     *
     * @return array<int, string>
     */
    public function getSentence(): array
    {
        return $this->sentence;
    }

    /**
     * Tells how many lines are needed for the given sentence.
     *
     * @return int
     */
    public function getLinesCount(): int
    {
        return count(
            $this->getSentence()
        );
    }
}
