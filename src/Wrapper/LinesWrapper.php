<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Wrapper;

use BitAndBlack\Helpers\ArrayHelper;

/**
 * @see \BitAndBlack\WordWrapper\Tests\Wrapper\LinesWrapperTest
 */
readonly class LinesWrapper implements WrapperInterface
{
    /**
     * @var array<int, string>
     */
    private array $sentence;

    public function __construct(int $linesCount, string $sentence, bool $allowUsingExistingHyphens = false)
    {
        $sentenceTrimmed = rtrim($sentence, ' ');

        $whitespacesAtEnd = substr(
            $sentence,
            strlen($sentenceTrimmed),
            strlen($sentence)
        );

        $splitCharacters = [
            '\s',
        ];

        if (true === $allowUsingExistingHyphens) {
            $splitCharacters[] = '-';
        }

        $pattern = '/(?<=' . implode('|', $splitCharacters) . ')/';
        $sentenceSplitted = preg_split($pattern, $sentenceTrimmed);

        /** @var array<int, string> $sentenceSplitted */
        $sentenceSplitted = ArrayHelper::getArray($sentenceSplitted);

        $charactersPerLine = strlen($sentence) / $linesCount;
        $charactersPerLineFloored = (int) floor($charactersPerLine);

        /**
         * @var array<int, array{
         *     pattern: string,
         *     offsetAdditionalBefore: int,
         *     offsetAdditionalAfter: int,
         *     limitLineBalancing: bool,
         * }> $patternSettings
         */
        $patternSettings = [
            [
                'pattern' => '/(?<=:)/',
                'offsetAdditionalBefore' => 2,
                'offsetAdditionalAfter' => 3,
                'limitLineBalancing' => true,
            ],
            [
                'pattern' => $pattern,
                'offsetAdditionalBefore' => 1,
                'offsetAdditionalAfter' => 2,
                'limitLineBalancing' => false,
            ],
        ];

        if (2 === $linesCount) {
            foreach ($patternSettings as $setting) {
                $lines = $this->balanceLines(
                    $sentenceTrimmed,
                    $charactersPerLineFloored,
                    $setting['pattern'],
                    $setting['offsetAdditionalBefore'],
                    $setting['offsetAdditionalAfter'],
                    $setting['limitLineBalancing'],
                );

                if ([] !== $lines) {
                    $this->sentence = $lines;
                    return;
                }
            }
        }

        $sentenceNewSplitted = [];
        $lineCounter = 0;

        foreach ($sentenceSplitted as $word) {
            if (!isset($sentenceNewSplitted[$lineCounter])) {
                $sentenceNewSplitted[$lineCounter] = '';
            }

            $sentenceNewSplitted[$lineCounter] .= $word;

            $charactersPerLineCurrent = mb_strlen($sentenceNewSplitted[$lineCounter]);

            if ($charactersPerLineCurrent > $charactersPerLine) {
                ++$lineCounter;
            }
        }

        $sentenceNewSplittedCount = count($sentenceNewSplitted);

        if ($sentenceNewSplittedCount > $linesCount) {
            $keyLast = $sentenceNewSplittedCount - 1;
            $keyPenultimate = $keyLast - 1;
            $sentenceNewSplitted[$keyPenultimate] .= $sentenceNewSplitted[$keyLast];
            unset($sentenceNewSplitted[$keyLast]);
        }

        $sentenceNewSplittedCount = count($sentenceNewSplitted);

        $keyLast = $sentenceNewSplittedCount - 1;
        $sentenceNewSplitted[$keyLast] = rtrim($sentenceNewSplitted[$keyLast], ' ') . $whitespacesAtEnd;
        
        $this->sentence = $sentenceNewSplitted;
    }

    /**
     * Returns an array of lines.
     *
     * @return array<int, string>
     */
    public function getSentence(): array
    {
        return $this->sentence;
    }

    /**
     * @param string $sentenceTrimmed
     * @param int $charactersPerLineFloored
     * @param string $patternColon
     * @param int $offsetAdditionalBefore
     * @param int $offsetAdditionalAfter
     * @param bool $limitLineBalancing
     * @return array<int, string>
     */
    private function balanceLines(
        string $sentenceTrimmed,
        int $charactersPerLineFloored,
        string $patternColon,
        int $offsetAdditionalBefore = 0,
        int $offsetAdditionalAfter = 0,
        bool $limitLineBalancing = false,
    ): array {
        $offset = -1;
        $lines = [];

        for ($counter = $charactersPerLineFloored; $counter > 0; --$counter) {
            ++$offset;

            if (true === $limitLineBalancing && $offset > $charactersPerLineFloored * .25) {
                break;
            }

            $charactersBefore = substr($sentenceTrimmed, $charactersPerLineFloored - $offset, 2);
            $charactersAfter = substr($sentenceTrimmed, $charactersPerLineFloored + $offset + 1, 2);

            $matchBeforeCount = preg_match($patternColon, $charactersBefore, $matchBefore, PREG_OFFSET_CAPTURE);
            $matchAfterCount = preg_match($patternColon, $charactersAfter, $matchAfter, PREG_OFFSET_CAPTURE);

            if (1 === $matchAfterCount && 1 === (int) $matchAfter[0][1]) {
                $lines[] = substr($sentenceTrimmed, 0, $charactersPerLineFloored + $offset + $offsetAdditionalAfter);
                $lines[] = substr($sentenceTrimmed, $charactersPerLineFloored + $offset + $offsetAdditionalAfter);
                break;
            }

            if (1 === $matchBeforeCount && 1 === (int) $matchBefore[0][1]) {
                $lines[] = substr($sentenceTrimmed, 0, $charactersPerLineFloored - $offset + $offsetAdditionalBefore);
                $lines[] = substr($sentenceTrimmed, $charactersPerLineFloored - $offset + $offsetAdditionalBefore);
                break;
            }
        }

        return $lines;
    }
}
