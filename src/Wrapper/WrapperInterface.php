<?php

/**
 * Bit&Black Word Wrapper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\WordWrapper\Wrapper;

interface WrapperInterface
{
    /**
     * Returns an array of lines.
     *
     * @return array<int, string>
     */
    public function getSentence(): array;
}
